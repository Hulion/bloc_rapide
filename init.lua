minetest.register_node("bloc_rapide:blocbloc", {
    description = "place un bloc de rapidite",
    tiles = {"face1.png"},
    -- pour casser le bloc:
    groups = {oddly_breakable_by_hand = 1},
    on_punch = function(pos, node, puncher, pointed_thing)
        local playerspeed = puncher:get_physics_override().speed
        if playerspeed > 1 then
            puncher:set_physics_override({
                speed = 1,
            })
        elseif playerspeed == 1 then
            puncher:set_physics_override({
                speed = 5,
            })
        end
    end,
})